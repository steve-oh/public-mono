import { assign, createMachine, spawn } from 'xstate';

import ThemePicker from './themePicker.machine';
import { TMachine as TThemePicker } from './themePicker.machine';

const MACHINE_ID = 'app';

const makeMachine = () =>
  createMachine(
    {
      tsTypes: {} as import('./app.machine.typegen').Typegen0,
      id: MACHINE_ID,
      schema: {
        context: {} as { themePicker: TThemePicker },
        events: {} as never,
      },
      context: {
        themePicker: null as unknown as TThemePicker,
      },
      initial: 'STATIC',
      states: {
        STATIC: {
          entry: ['initialize'],
        },
      },
    },
    {
      actions: {
        initialize: assign({
          themePicker: () =>
            spawn(ThemePicker.machine, {
              name: ThemePicker.MACHINE_ID,
            }),
        }),
      },
    }
  );

export default {
  MACHINE_ID,
  makeMachine,
};
