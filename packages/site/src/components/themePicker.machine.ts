import * as XState from 'xstate';

export type Command =
  | { type: 'SELECT_DARK' }
  | { type: 'SELECT_LIGHT' }
  | { type: 'SELECT_SOLARIZED' }
  | { type: 'SELECT_STANDARD' };

const MACHINE_ID = 'theme-picker';

enum Color {
  ACCENT = '--accent',
  ALT_ACCENT = '--alt-accent',
  PRIMARY_CONTENT = '--primary-content',
  SECONDARY_CONTENT = '--secondary-content',
  EMPHASIZED_CONTENT = '--emphasized-content',
  BACKGROUND = '--background',
  BACKGROUND_HIGHLIGHT = '--background-highlight',
  TOOLBAR = '--toolbar',
  BLOCKQUOTE_BORDER = '--blockquote-border',
}

interface Theme {
  [Color.ACCENT]: string;
  [Color.ALT_ACCENT]: string;
  [Color.PRIMARY_CONTENT]: string;
  [Color.SECONDARY_CONTENT]: string;
  [Color.EMPHASIZED_CONTENT]: string;
  [Color.BACKGROUND]: string;
  [Color.BACKGROUND_HIGHLIGHT]: string;
  [Color.TOOLBAR]: string;
  [Color.BLOCKQUOTE_BORDER]: string;
}

type ThemeId = { LUMINANCE: string; SCHEME: string };

const toString = (themeId: ThemeId): string =>
  `${themeId.LUMINANCE}-${themeId.SCHEME}`;

const LUMINANCE = 'LUMINANCE';

const DARK = 'DARK';
const LIGHT = 'LIGHT';

const SCHEME = 'SCHEME';

const SOLARIZED = 'SOLARIZED';
const STANDARD = 'STANDARD';

const DARK_SOLARIZED = toString({ LUMINANCE: DARK, SCHEME: SOLARIZED });
const LIGHT_SOLARIZED = toString({ LUMINANCE: LIGHT, SCHEME: SOLARIZED });
const DARK_STANDARD = toString({ LUMINANCE: DARK, SCHEME: STANDARD });
const LIGHT_STANDARD = toString({ LUMINANCE: LIGHT, SCHEME: STANDARD });

type ThemeKey =
  | typeof DARK_SOLARIZED
  | typeof LIGHT_SOLARIZED
  | typeof DARK_STANDARD
  | typeof LIGHT_STANDARD;

const Themes = new Map<ThemeKey, Theme>();

Themes.set(DARK_SOLARIZED, {
  [Color.ACCENT]: '#268BD2',
  [Color.ALT_ACCENT]: '#2AA198',
  [Color.PRIMARY_CONTENT]: '#839496',
  [Color.SECONDARY_CONTENT]: '#586E75',
  [Color.EMPHASIZED_CONTENT]: '#93A1A1',
  [Color.BACKGROUND]: '#002B36',
  [Color.BACKGROUND_HIGHLIGHT]: '#073642',
  [Color.TOOLBAR]: '#657B83',
  [Color.BLOCKQUOTE_BORDER]: '#268BD233',
});

Themes.set(LIGHT_SOLARIZED, {
  [Color.ACCENT]: '#b58900',
  [Color.ALT_ACCENT]: '#CB4B16',
  [Color.PRIMARY_CONTENT]: '#657b83',
  [Color.SECONDARY_CONTENT]: '#93a1a1',
  [Color.EMPHASIZED_CONTENT]: '#586e75',
  [Color.BACKGROUND]: '#fdf6e3',
  [Color.BACKGROUND_HIGHLIGHT]: '#eee8d5',
  [Color.TOOLBAR]: '#839496',
  [Color.BLOCKQUOTE_BORDER]: '#b5890033',
});

Themes.set(DARK_STANDARD, {
  [Color.ACCENT]: '#926F4A',
  [Color.ALT_ACCENT]: '#619B97',
  [Color.PRIMARY_CONTENT]: '#B0CCCA',
  [Color.SECONDARY_CONTENT]: '#8AB4B1',
  [Color.EMPHASIZED_CONTENT]: '#D8E6E5',
  [Color.BACKGROUND]: '#002827',
  [Color.BACKGROUND_HIGHLIGHT]: '#003D3B',
  [Color.TOOLBAR]: '#36837E',
  [Color.BLOCKQUOTE_BORDER]: '#926F4A33',
});

Themes.set(LIGHT_STANDARD, {
  [Color.ACCENT]: '#A46466',
  [Color.ALT_ACCENT]: '#006B67',
  [Color.PRIMARY_CONTENT]: '#003D3B',
  [Color.SECONDARY_CONTENT]: '#005451',
  [Color.EMPHASIZED_CONTENT]: '#002827',
  [Color.BACKGROUND]: 'white',
  [Color.BACKGROUND_HIGHLIGHT]: '#D8E6E5',
  [Color.TOOLBAR]: '#36837E',
  [Color.BLOCKQUOTE_BORDER]: '#A4646633',
});

const machine = XState.createMachine(
  {
    tsTypes: {} as import('./themePicker.machine.typegen').Typegen0,
    id: MACHINE_ID,
    schema: {
      events: {} as Command,
    },
    type: 'parallel',
    states: {
      LUMINANCE: {
        initial: LIGHT,
        states: {
          LIGHT: {
            entry: ['notifyLight', 'updateTheme'],
            on: {
              SELECT_DARK: {
                target: DARK,
              },
            },
          },
          DARK: {
            entry: ['notifyDark', 'updateTheme'],
            on: {
              SELECT_LIGHT: {
                target: LIGHT,
              },
            },
          },
        },
      },
      SCHEME: {
        initial: STANDARD,
        states: {
          STANDARD: {
            entry: ['notifyStandard', 'updateTheme'],
            on: {
              SELECT_SOLARIZED: {
                target: SOLARIZED,
              },
            },
          },
          SOLARIZED: {
            entry: ['notifySolarized', 'updateTheme'],
            on: {
              SELECT_STANDARD: {
                target: STANDARD,
              },
            },
          },
        },
      },
    },
  },
  {
    actions: {
      notifyDark: XState.sendParent('DARK_WAS_SELECTED'),
      notifyLight: XState.sendParent('LIGHT_WAS_SELECTED'),
      notifySolarized: XState.sendParent('SOLARIZED_WAS_SELECTED'),
      notifyStandard: XState.sendParent('STANDARD_WAS_SELECTED'),
      updateTheme: {
        type: 'updateTheme',
        exec: (_, __, meta) => {
          const key = toString(meta.state.value as ThemeId) as ThemeKey;
          const theme = Themes.get(key);
          if (theme) {
            Object.keys(theme).forEach((key) => {
              document.documentElement.style.setProperty(
                key,
                theme[key as Color]
              );
            });
          }
        },
      },
    },
  }
);

export type TMachine = XState.ActorRefFrom<typeof machine>;
export type TState = XState.StateFrom<typeof machine>;
export type TSend = XState.Sender<Command>;

const isDark = (state: TState) => state.matches(`${LUMINANCE}.${DARK}`);
const isLight = (state: TState) => state.matches(`${LUMINANCE}.${LIGHT}`);
const isSolarized = (state: TState) => state.matches(`${SCHEME}.${SOLARIZED}`);
const isStandard = (state: TState) => state.matches(`${SCHEME}.${STANDARD}`);

const selectDark = (send: TSend) => {
  send('SELECT_DARK');
};

const selectLight = (send: TSend) => {
  send('SELECT_LIGHT');
};

const selectSolarized = (send: TSend) => {
  send('SELECT_SOLARIZED');
};

const selectStandard = (send: TSend) => {
  send('SELECT_STANDARD');
};

export default {
  MACHINE_ID,

  machine,

  isDark,
  isLight,
  isSolarized,
  isStandard,

  selectDark,
  selectLight,
  selectSolarized,
  selectStandard,
};
