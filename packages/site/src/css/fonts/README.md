Several of the fonts used in this project are not free. They are included in the encrypted `fonts.zip` and expanded at build time to allow the CI/CD pipeline to run.

If you want to use these fonts in your fork of this project, you will need to purchase licenses to use them from:

[Connary Fagen, Inc.](https://connary.com/)

The typefaces used are:
- [Artifex Hand](https://connary.com/artifexhand.html) (body)
- [Visby Round](https://connary.com/visbyround.html) (titles)
- [Cartograph](https://connary.com/cartograph.html) (logotype only)
